	<!DOCTYPE html>
	<html lang="zxx" class="no-js">

	<?php include 'partials/head.php';?>

		<body>
			<!-- start banner Area -->
			<section class="banner-area" id="home">
					<?php include 'partials/header.php';?>
			</section>

			<section class="default-banner active-blog-slider">
					<div class="item-slider relative" style="background: url(img/slider1.jpg);background-size: cover;">
						<div class="overlay" style="background: rgba(0,0,0,.3)"></div>
						<div class="container">
							<div class="row fullscreen justify-content-center align-items-center">
								<div class="col-md-10 col-12">
									<div class="banner-content text-center">
										<h4 class="text-white mb-20 text-uppercase">Discover the Colorful World</h4>
										<h1 class="text-uppercase text-white">New Adventure</h1>
										<p class="text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temp <br>
										or incididunt ut labore et dolore magna aliqua. Ut enim ad minim.</p>
										<a href="#" class="text-uppercase header-btn">Discover Now</a>
									</div>
								</div>

							</div>
						</div>
					</div>
					<div class="item-slider relative" style="background: url(img/slider2.jpg);background-size: cover;">
						<div class="overlay" style="background: rgba(0,0,0,.3)"></div>
						<div class="container">
							<div class="row fullscreen justify-content-center align-items-center">
								<div class="col-md-10 col-12">
									<div class="banner-content text-center">
										<h4 class="text-white mb-20 text-uppercase">Discover the Colorful World</h4>
										<h1 class="text-uppercase text-white">New Trip</h1>
										<p class="text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temp <br>
										or incididunt ut labore et dolore magna aliqua. Ut enim ad minim.</p>
										<a href="#" class="text-uppercase header-btn">Discover Now</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="item-slider relative" style="background: url(img/slider3.jpg);background-size: cover;">
						<div class="overlay" style="background: rgba(0,0,0,.3)"></div>
						<div class="container">
							<div class="row fullscreen justify-content-center align-items-center">
								<div class="col-md-10 col-12">
									<div class="banner-content text-center">
										<h4 class="text-white mb-20 text-uppercase">Discover the Colorful World</h4>
										<h1 class="text-uppercase text-white">New Experience</h1>
										<p class="text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temp <br>
										or incididunt ut labore et dolore magna aliqua. Ut enim ad minim.</p>
										<a href="#" class="text-uppercase header-btn">Discover Now</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				
			<!-- start filosofia y objetivo Area -->		
			<?php include 'pages/filosofia-objetivo.php';?> <br>
			<!-- end filosofia y objetivo Area -->	
			
			<!-- start servicios Area -->		
			<?php include 'pages/servicios.php';?> <br>
			<!-- end servicios Area -->	

			<!-- start equipo tekne Area -->		
			<?php include 'pages/equipo-tekne.php';?> <br>
			<!-- end equipo tekne Area -->	

			<!-- start contacto y redes sociales Area -->		
			<?php include 'pages/ubicacion.php';?> <br>
			<!-- end contacto y redes sociales Area -->

			<!-- start contacto y redes sociales Area -->		
			<?php include 'pages/contacto.php';?> <br>
			<!-- end contacto y redes sociales Area -->	

			<!-- start footer Area -->		
			<?php include 'partials/footer.php';?> <br>
			<!-- end footers Area -->	

		</body>
	</html>